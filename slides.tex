\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=Download management on metered connections,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}

% Abstract here: https://2018.guadec.org/pages/talks-and-events.html#abstract-4-download_management_on_metered_connections
\title{Download management on metered connections}

\author{Philip Withnall\\Endless Mobile\\\texttt{philip@tecnocode.co.uk}}
\date{July 8, 2018}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 15 minutes for talk, 10 minutes for questions.}


\begin{frame}{What’s the problem?}
\begin{itemize}
  \item{Many people use metered internet connections}
  \item{Cannot download everything they want to, when they want to}
  \item{Downloads have to be prioritised}
\end{itemize}
\end{frame}

\note{Many people, especially in non-western countries, use metered internet connections where they get charged for the bandwidth they use.

This means they cannot download everything they want to, when they want to; and often have to be quite careful and in-control of what they download each month.

In other words, their downloads have to be prioritised, especially if those downloads are quite large. For example, operating system and app updates.

As they are part of our target market, Endless has been quite interested in making it easier for people with such internet connections to download what they want, without causing huge bandwidth bills.

It’s worth noting that what we are discussing here only applies to ‘big’ downloads, and is a voluntary service for software components to use — this is not a network firewall or enforced queuing system.}


\begin{frame}{What is metering?}
\begin{itemize}
  \item{Pay per unit bandwidth used}
  \item{Pricing structures vary a lot}
  \item{Could have unmetered periods of the day}
  \item{Could be non-net-neutral}
\end{itemize}
\end{frame}

\note{A little more detail on what metered connections are. They are connections where you pay per unit of bandwidth used, rather than paying a fixed price per month for unlimited bandwidth.

However, pricing structures vary a lot. Some tariffs may allow unlimited downloads at a certain time of day (for example, early in the morning, for OS updates); or they may always allow downloads
from certain websites for free (this is not net-neutral). Each network provider may have several tariffs, and the tariffs may change regularly (with promotions, for example).}


\begin{frame}{Does it affect me?}
\begin{itemize}
  \item{Home/Work internet connection may be unlimited}
  \item{Trains, public Wi-Fi, mobile internet often aren’t}
  \item{Main internet connection may be bandwidth constrained, even if unlimited}
  \item{Downloads can be prioritised}
\end{itemize}
\end{frame}

\note{This kind of thing affects western countries too, although in less pervasive ways. While it’s typical to have an unlimited home or work internet connection,
metered connections are encountered on trains (in the UK), public Wi-Fi hotspots (commonly in airports, where you pay for an hour’s internet access), or with many
mobile internet tariffs.

There are other ways a connection can benefit from prioritising downloads: if the connection is slow, the number of parallel downloads should be limited, and the
queue of downloads prioritised to complete important ones first. You might want certain downloads to not be started until you’re back home on a fast connection.}


\begin{frame}{Introducing Mogwai}
\begin{itemize}
  \item{Mogwai is a download scheduler}
  \item{It does downloads after midnight (hence the name)
    \begin{itemize}
      \item{\small{It’s also a daemon, which ties in with the Cantonese meaning}}
    \end{itemize}
  }
  \item{Applications ask it when to start ‘big’ downloads}
  \item{It schedules all the requests and notifies the applications}
  \item{Uses various configuration, including tariff files}
\end{itemize}
\end{frame}

\note{So we wrote Mogwai. It’s a download scheduler: applications which want to do ‘big’ downloads tell the scheduler what they want to download (including its size,
domain name, etc.), the scheduler schedules all the requests, and signals each application when it’s time to start downloading. The scheduler can later pre-empt a
download and ask the application to pause downloading for a bit, too.

The scheduling decisions are based on a tariff file for each network connection, which says what period of day that connection is metered, what period of day it’s
unmetered, and some recurrence rules for when those periods recur. A connection may be metered all the time, or unmetered all the time. The tariff format is extensible,
so other properties could be added in future (for example, downloads on a certain domain are unmetered during this period).}

\note{Other preferences (stored in NetworkManager on each network connection) are taken into account, such as whether to always allow downloads on that connection.

Mogwai is implemented as a D-Bus service on the system bus, with a client library and some helper utilities provided.}


\begin{frame}{What Mogwai is not}
\begin{itemize}
  \item{Not security — it’s an opt-in system}
  \item{Not currently monitoring bandwidth usage}
\end{itemize}
\end{frame}

\note{As mentioned earlier, Mogwai does not intend to provide security. It cannot stop applications doing big downloads whenever they like — the applications have to
opt in to querying Mogwai about whether to download, and can ignore what Mogwai tells them.

It doesn’t (currently) monitor bandwidth usage either, so its calculations of how much bandwidth has been used during a particular tariff period is entirely blind.
If a tariff period is limited to 1GB of downloads, Mogwai has to base its scheduling decisions on the total amount of bandwidth requested by applications, and assume
that only a nominal amount of bandwidth is used otherwise. Clearly, improvement can be made here.}


\begin{frame}{How do I use it?}
\begin{itemize}
  \item{\texttt{libmogwai-schedule-client}}
  \item{See \href{https://gitlab.freedesktop.org/pwithnall/mogwai/blob/c001a82fe74c77cf4f1dc6ec5733cf3abfc3552c/mogwai-schedule-client/main.c\#L153}{source code for \texttt{mogwai-schedule-client} utility}}
  \item{Or the ‘Automatic Updates’ panel in gnome-control-center}
\end{itemize}
\end{frame}

\note{Mogwai is currently used in Endless OS, in our fork of gnome-software, and in our OS updater (eos-updater). We have changes to gnome-control-center to add it to the
‘automatic updates’ panel.

If you want to use it, use the API from \texttt{libmogwai-schedule-client}, which is a client for the D-Bus API exported by the Mogwai daemon. There’s an example in the
code for \texttt{mogwai-schedule-client}, which is a simple download client shipped with Mogwai. See the PDF of these slides for a link; they’ll be available afterwards.

Essentially, your application needs to create a schedule entry for each download, and tie the state of the download to signals from the schedule entry telling it when to
start, pause, and resume downloading.}


\begin{frame}{Mogwai in gnome-control-center}
\begin{center}
\includegraphics[keepaspectratio=true,width=0.7\textwidth]{gnome-control-center.png}
\end{center}
\end{frame}


\begin{frame}{Future work — available for mentoring}
\begin{itemize}
  \item{Monitor bandwidth usage to close the feedback loop}
  \item{Provide download statistics to GNOME Usage}
  \item{Account for more factors when scheduling downloads}
\end{itemize}
\end{frame}

\note{What’s left to do? Lots. The next big task on the list is to add a monitoring daemon to Mogwai, which would monitor bandwidth usage per app, and store statistics on usage
based on this. Those statistics could be used in GNOME Usage, and could be used by the scheduler to work out when the bandwidth limit for the current tariff period has been reached.

I have a plan for how this can be implemented efficiently, using nftables and the systemd unit hierarchy.

The scheduler can always be made more clever, taking more factors into account from the system, and from the schedule entries provided by applications.

I’m willing to mentor people to implement this functionality in Mogwai, since my time on it at work has been cut.}


\begin{frame}{Miscellany}
\begin{description}
  \item[GitLab]{\url{https://gitlab.freedesktop.org/pwithnall/mogwai}}
\end{description}

% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2018}}
\end{center}
\end{frame}

\end{document}
